package de.ak.labapp;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;


@SuppressWarnings("deprecation")
public class bioHttp{
	
	private final String TAG = "BioHttp";		// Tag zur genauern Indentifikation der Activity f�rs mitloggen
    public String domain = "http://192.168.1.113:80/";
    public String csrftoken = "";
    public String session = "";
    public String sample = "";
    private String add = "";
    private String html = "";
    
    private String[] analyse1 = {"Tara", "EW", "AW", "Result"};
    private String[] analyse2 = {"Tara", "EW", "AW", "Result"};
    private String[] analyse3 = {"EW", "H2SO4", "Result"};
    private String[] analyse4 = {"EW", "H2SO4", "Result"};
    private String[] analyse5 = {"ph", "Redox", "Result"};
    private String[] be = {"Ho_BW", "EW", "Dratrest", "Tara", "AW", "Result"};
    private String[] chnso = {"C_TS", "H_TS", "N_TS", "S_TS", "O_TS", "C_oTS", "H_oTS", "N_oTS", "S_oTS", "O_oTS", "Result"};
    private String[] csb = {"O2kg", "Result"};
    private String[] fettsaeuren = {"HAC", "PRO", "i_BUT", "n_BUT", "i_VAL", "n_VAL", "CAP", "HAC_PRO", "Anmerkung", "Result"};
    private String[] karlfischer = {"EW", "Wkf", "Wassergehalt", "V_methanol", "Result"};
    private String[] silagequalitaet = {"Milchsaeure", "Essigsaeure", "Buttersaeure", "Gesamtsaeure", "pH_Wert", "NH4_N", "Punkte", "Note", "Result"};
    
    private Map<String, String[]> analysis = new HashMap<String, String[]>();
        
    bioHttp(String csrf, String sess, String sam){
    	
        csrftoken = csrf;
        session = sess;
        sample = sam;
        Log.i(TAG, "global csrf:" + csrftoken);
        Log.i(TAG, "global session:" + session);
        Log.i(TAG, "global sample:" + sample);
        
        analysis.put("Analyse1", analyse1);
        analysis.put("Analyse2", analyse2);
        analysis.put("Analyse3", analyse3);
        analysis.put("Analyse4", analyse4);
        analysis.put("Analyse5", analyse5);
        analysis.put("BE", be);
        analysis.put("CHNSO", chnso);
        analysis.put("CSB", csb);
        analysis.put("Fettsaeuren", fettsaeuren);
        analysis.put("KarlFischer", karlfischer);
        analysis.put("Silagequalitaet", silagequalitaet);
    }
    
    bioHttp(){
    	
    	Log.i(TAG, "normaler Konstrucktor");
    	
    	analysis.put("Analyse1", analyse1);
        analysis.put("Analyse2", analyse2);
        analysis.put("Analyse3", analyse3);
        analysis.put("Analyse4", analyse4);
        analysis.put("Analyse5", analyse5);
        analysis.put("BE", be);
        analysis.put("CHNSO", chnso);
        analysis.put("CSB", csb);
        analysis.put("Fettsaeuren", fettsaeuren);
        analysis.put("KarlFischer", karlfischer);
        analysis.put("Silagequalitaet", silagequalitaet);
       
    }
    
    public int sendGet(boolean redirect){
        
        /*HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(domain+add);
        HttpResponse response = null;
        
        HttpParams params = client.getParams();
        HttpClientParams.setRedirecting(params, false);
        
        request.addHeader("Cookie", "csrftoken="+csrftoken+"; d_session="+session);
        
        try {
            response = client.execute(request);
        } catch (IOException e) {
            return -1;
        }*/
        
        HttpResponse response = null;
        
        Get get = new Get();
        Log.i("TEST","nach new Get");
        try {
            response = get.execute(redirect).get();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.i("TEST","nach new execute");   
        
        if (response != null){
            if (response.containsHeader("Set-Cookie")){
                Header[] h;
                h = response.getHeaders("Set-Cookie");
                
                String s1[];
                String s2[];
                
                if(h[0].toString().contains("csrftoken=")){
                    s1 = h[0].toString().split("csrftoken=");
                    s2 = (s1[1].split(";"));
                    csrftoken = s2[0];
                }
                
                if(h[0].toString().contains("sample=")){
                    s1 = h[0].toString().split("sample=");
                    s2 = (s1[1].split(";"));
                    sample = s2[0];
                }
                
            }
            else{
                return -3;
            }
        
            return 0;
        }
        else{
            Log.i("TEST","nach if");
            return -6;
        }
    }
    
    public int sendPost(List<NameValuePair> urlParameters){
        HttpResponse response = null;
        Post post = new Post();
        
        try {
            response = post.execute(urlParameters).get();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
           
        if(response != null){
            if (response.containsHeader("Set-Cookie")){
                Header[] h;
                h = response.getHeaders("Set-Cookie");
                
                String s1[];
                String s2[];
                
                s1 = h[0].toString().split("d_session=");
                if(s1.length > 1){
                    s2 = (s1[1].split(";"));
                    session = s2[0];
                }
            }
            else{
                return -3;
            }
            
            return 0;
        }
        else{
            return -6;
        }
    }

    public int login(String usr, String pwd){
        int get, post;
        
        csrftoken = "";
        session = "";
        sample = "";
        
        add = "login/";
        get = sendGet(false);
        
        if(get != 0)
            return get;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("username", usr));
        urlParameters.add(new BasicNameValuePair("password", pwd));
        
        add = "login/";
        post = sendPost(urlParameters);
        
        if(post != 0)
            return post;
        
        if(session!="" && csrftoken!="")
            return 0;
        else
            return -5;
    }
    
    public int search_sample(String samp){
        int get;
        
        sample = "";
        
        add = "search_sample/?samp_id="+samp;
        get = sendGet(false);
        
        if(get != 0)
            return get;
        
        if(session!="" && csrftoken!="" && sample != "")
            return 0;
        else
            return -5;
    }
    
    public int setAnalyse1(int num, String tara, String ew, String aw){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("Tara:"+num, tara));
        urlParameters.add(new BasicNameValuePair("EW:"+num, ew));
        urlParameters.add(new BasicNameValuePair("AW:"+num, aw));
        
        add = "input/Analyse1/";
        post = sendPost(urlParameters);
        
        return post;
    }
        
    public int setAnalyse2(int num, String tara, String ew, String aw){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("Tara:"+num, tara));
        urlParameters.add(new BasicNameValuePair("EW:"+num, ew));
        urlParameters.add(new BasicNameValuePair("AW:"+num, aw));
        
        add = "input/Analyse2/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setAnalyse3(int num, String ew, String h2so4){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("EW:"+num, ew));
        urlParameters.add(new BasicNameValuePair("H2SO4:"+num, h2so4));
        
        add = "input/Analyse3/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setAnalyse4(int num, String ew, String h2so4){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("EW:"+num, ew));
        urlParameters.add(new BasicNameValuePair("H2SO4:"+num, h2so4));
        
        add = "input/Analyse4/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setAnalyse5(int num, String ph, String redox){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("ph:"+num, ph));
        urlParameters.add(new BasicNameValuePair("Redox:"+num, redox));
        
        add = "input/Analyse5/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setBe(int num, String ho_wo, String ew, String dra, String tara, String aw){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("Ho_BW:"+num, ho_wo));
        urlParameters.add(new BasicNameValuePair("EW:"+num, ew));
        urlParameters.add(new BasicNameValuePair("Dratrest:"+num, dra));
        urlParameters.add(new BasicNameValuePair("Tara:"+num, tara));
        urlParameters.add(new BasicNameValuePair("AW:"+num, aw));
        
        add = "input/BE/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setchnso(int num, String c_ts, String h_ts, String n_ts, String s_ts, String o_ts, String c_ots, String h_ots, String n_ots, String s_ots, String o_ots){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("C_TS:"+num, c_ts));
        urlParameters.add(new BasicNameValuePair("H_TS:"+num, h_ts));
        urlParameters.add(new BasicNameValuePair("N_TS:"+num, n_ts));
        urlParameters.add(new BasicNameValuePair("S_TS:"+num, s_ts));
        urlParameters.add(new BasicNameValuePair("O_TS:"+num, o_ts));
        urlParameters.add(new BasicNameValuePair("C_oTS:"+num, c_ots));
        urlParameters.add(new BasicNameValuePair("H_oTS:"+num, h_ots));
        urlParameters.add(new BasicNameValuePair("N_oTS:"+num, n_ots));
        urlParameters.add(new BasicNameValuePair("S_oTS:"+num, s_ots));
        urlParameters.add(new BasicNameValuePair("O_oTS:"+num, o_ots));
        
        add = "input/CHNSO/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setCsb(int num, String o2kg){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("O2kg:"+num, o2kg));
        
        
        add = "input/CSB/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setfatty(int num, String hac, String pro, String ibut, String nbut, String ival, String nval, String cap, String hacpro, String note){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("HAC:"+num, hac));
        urlParameters.add(new BasicNameValuePair("PRO:"+num, pro));
        urlParameters.add(new BasicNameValuePair("i_BUT:"+num, ibut));
        urlParameters.add(new BasicNameValuePair("n_BUT:"+num, nbut));
        urlParameters.add(new BasicNameValuePair("i_VAL:"+num, ival));
        urlParameters.add(new BasicNameValuePair("n_VAL:"+num, nval));
        urlParameters.add(new BasicNameValuePair("CAP:"+num, cap));
        urlParameters.add(new BasicNameValuePair("HAC_PRO:"+num, hacpro));
        urlParameters.add(new BasicNameValuePair("Anmerkung:"+num, note));
        
        add = "input/Fettsaeuren/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setsilage(int num, String milch, String essig, String butter, String ges, String ph, String nh4, String points, String note){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("Milchsaeure:"+num, milch));
        urlParameters.add(new BasicNameValuePair("Essigsaeure:"+num, essig));
        urlParameters.add(new BasicNameValuePair("Buttersaeure:"+num, butter));
        urlParameters.add(new BasicNameValuePair("Gesamtsaeure:"+num, ges));
        urlParameters.add(new BasicNameValuePair("pH_Wert:"+num, ph));
        urlParameters.add(new BasicNameValuePair("NH4_N:"+num, nh4));
        urlParameters.add(new BasicNameValuePair("Punkte:"+num, points));
        urlParameters.add(new BasicNameValuePair("Note:"+num, note));
        
        add = "input/Silagequalitaet/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public int setKarli(int num, String ew, String wkf, String wg, String vm){
        int post;
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("EW:"+num, ew));
        urlParameters.add(new BasicNameValuePair("Wkf:"+num, wkf));
        urlParameters.add(new BasicNameValuePair("Wassergehalt:"+num, wg));
        urlParameters.add(new BasicNameValuePair("V_methanol:"+num, vm));
        
        add = "input/KarlFischer/";
        post = sendPost(urlParameters);
        
        return post;
    }
    
    public String[] getPlaceholder(int num, String key){
        int get;
        String s1[];
        String s2[];
        String values[] = new String[20];
        
        add = "input/"+key+"/";
        get = sendGet(true);
        int i;
        for(i=0; i < analysis.get(key).length-1; i++){
            System.out.println(analysis.get(key)[i]);
            s1 = html.split("name=\""+analysis.get(key)[i]+":"+num+"\" placeholder=\"");
            s2 = s1[1].split("\" type=\"text\"");
            values[i] = s2[0];
        }
        
        s1 = html.split("<p class=\"text_out\">");
        values[i] = s1[num].replaceAll("(?s)</p>.*","");
        
        return values;
    }
    
    private class Get extends AsyncTask<Boolean, Void, HttpResponse>{
        
        @Override
        protected HttpResponse doInBackground(Boolean... params) {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(domain+add);
            HttpResponse response = null;
            
            HttpParams param = client.getParams();
            HttpClientParams.setRedirecting(param, params[0]);
            
            request.addHeader("Cookie", "csrftoken="+csrftoken+"; d_session="+session+"; sample="+sample);
            
            try {
                response = client.execute(request);
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                Log.i("TEST","protocol");
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.i("TEST","io");
                e.printStackTrace();
                return null;
            }
            
            try {
                html = EntityUtils.toString(response.getEntity());
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            return response;
        }
        
        
    }


    private class Post extends AsyncTask<List, Void, HttpResponse>{

        @Override
        protected HttpResponse doInBackground(List... params) {
            HttpClient client = new DefaultHttpClient();
            HttpPost request = new HttpPost(domain+add);
            HttpResponse response = null;
            
            HttpParams param = client.getParams();
            HttpClientParams.setRedirecting(param, false); 
            
            request.addHeader("Cookie", "csrftoken="+csrftoken+"; d_session="+session+"; sample="+sample);
            
            params[0].add(new BasicNameValuePair("csrfmiddlewaretoken", csrftoken));
            
            try {
                request.setEntity(new UrlEncodedFormEntity(params[0]));
            } catch (UnsupportedEncodingException e1) {
                return null;
            }

            try {
                response = client.execute(request);
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
            
            return response;
        }
        
        
    }
}
