package de.ak.labapp;

import de.ak.labapp.view.viewgroup.FlyOutContainer;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class wellcome extends Activity {
	
	
	private final String TAG = "WellcomeActivity";		// Tag zur genauern Indentifikation der Activity f�rs mitloggen
	public String csrf;
	public String sample;
	public String session;
	final Context context = this;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wellcomeactivity);
        Log.i(TAG, "on Create");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
    	MenuInflater inflate = getMenuInflater();
        getMenuInflater().inflate(R.menu.wellcome_menu, menu);
        return true;
    }
    
///////////////////////////////////Start/Stopp/Resume/restart/pause f�r die Activitys der Klasse//////////////////
///////////////////////////////////Sind hier angef�hr um mit zu Loggen!///////////////////////////////////////////
    @Override
    public void onStart(){
    	
    	super.onStart();
    	Log.i(TAG, "onStart");			//Log Befehl zum mitloggen
    }
    @Override
    public void onStop(){
    	
    	super.onStop();
    	Log.i(TAG, "onStopped");		//Log Befehl zum mitloggen
    } 
    @Override
    public void onResume(){
    	
    	super.onResume();
    	Log.i(TAG, "onResume");				//Log Befehl zum mitloggen
    }
    @Override
    public void onRestart(){
    	
    	super.onRestart();
    	Log.i(TAG, "onRestart");			//Log Befehl zum mitloggen
    }
    @Override
    public void onPause(){
    	
    	super.onPause();
    	Log.i(TAG, "onPause");			//Log Befehl zum mitloggen
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    @Override
	public boolean onOptionsItemSelected(MenuItem item){
		///////////////Initialisierung des Men�s////////////
		switch (item.getItemId()) {
	        
	    	case R.id.logout_menu :
	    		
	    		Log.i(TAG, "Logout is clicked");
	    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	    		// set title
	    		alertDialogBuilder.setTitle("Logout?");
	    		// set dialog message
	    		alertDialogBuilder
	    		.setMessage("Click yes to Logout!")
	    		.setCancelable(false)
	    		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    			// if this button is clicked, close
	    			// current activity
	    			finish();
	    			}
	    		})
	    		.setNegativeButton("No",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    				// if this button is clicked, just close
	    				// the dialog box and do nothing
	    						dialog.cancel();
	    				}
	    		});
	    		// create alert dialog
	    		AlertDialog alertDialog = alertDialogBuilder.create();
	    		// show it
	    		alertDialog.show();
	    		return true;
	    		
	        case R.id.info_menu :
	            return true;
	            
	        default:
	        	return super.onOptionsItemSelected(item);
		}
    }
    
	public void onClick_scanbutton(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r den scanbutton vom Layout(wellcome_menu)							//
	//ruft die Activity vom Barcodescanner auf (http://zxing.org/)							//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		
		IntentIntegrator integrator = new IntentIntegrator(this);
		integrator.initiateScan();
		
		
	}
	
	public void onClick_searchbutton(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r den searchbutton vom Layout(wellcome_menu)						//
	//																						//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
	
		EditText probenID;
		probenID = (EditText) findViewById(R.id.searchID);
		String probenid = probenID.getText().toString();
		
		Log.i(TAG, probenid);
		getIntdata();
		
		bioHttp biohttp = new bioHttp(csrf, session, sample);
		int check;
		
		check = biohttp.search_sample(probenid);
		Log.i(TAG, "check: " + check);
		
		if(check == 0){
			Intent start_analyse1 = new Intent(this, analyse1Activity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
			finish();
			start_analyse1.putExtra("probenID",probenid);
			start_analyse1.putExtra("new",1);
			startActivity(start_analyse1);									// neue Activity starten
		    Log.i(TAG,"neue Activity starten(analyse1)");			// Informationslog			  
		}else{
			
			Log.i(TAG, "Sample Fail");
            EditText search = (EditText) findViewById(R.id.searchID);
            search.setHint("Samplen ID");
            search.setText("");
     	   	TextView fail = (TextView) findViewById(R.id.note);
     	    fail.setText("Sample not found");
		}
		
	}
	

		public void onActivityResult(int requestCode, int resultCode, Intent intent) {
			  IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
			  if (scanResult != null) {
				String qrcode;
				String codetype;
				qrcode = scanResult.getContents();
				codetype = scanResult.getFormatName();
				Log.i(TAG,codetype);
				Log.i(TAG, "probenID: " + qrcode);
				
				getIntdata();
				
				bioHttp biohttp = new bioHttp(csrf, session, qrcode);
				int check;
				
				check = biohttp.search_sample(qrcode);
				Log.i(TAG, "check: " + check);
				if(check == 0){
					Intent start_analyse1 = new Intent(this, analyse1Activity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
					finish();
					start_analyse1.putExtra("probenID",qrcode);
					start_analyse1.putExtra("new",1);
					startActivity(start_analyse1);									// neue Activity starten
				    Log.i(TAG,"neue Activity starten(analyse1)");			// Informationslog			  
				}
			}	
		}
		
		public void getIntdata(){
			
			globalFunktion setdata = ((globalFunktion)getApplicationContext());
	        csrf = setdata.getcsrftoken();
	        Log.i(TAG, "Csrf:" + csrf);
	        session = setdata.getsession();
	        Log.i(TAG, "Session:" + session);
	        sample = setdata.getprobenID();
	        Log.i(TAG, "Sample:" + sample);
			
		}
}


