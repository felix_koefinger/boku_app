package de.ak.labapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;


public class http2 extends AsyncTask<Void, Void, Void>{
    
    public String domain = "http://192.168.1.117:80/login/";
    public String csrftoken = "";
    public String TAG = "http2";
    
    public void sendGet()
    {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(domain);
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    
        // Get the response
        BufferedReader rd = null;
        try {
            rd = new BufferedReader
              (new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch(IllegalStateException e){
        	e.printStackTrace();
        }
        
        Header[] h;
        //h = response.getHeaders("Set-Cookie");
        
        h = response.getAllHeaders();
        
        System.out.println(h[4].toString());
        for(int i=0; i < h.length; i++)
        {
        	Log.i(TAG, h[i].toString());
        }
        
        h = response.getHeaders("Set-Cookie");
        String csrf1[];
        String csrf2[];
        
        csrf1 = h[0].toString().split("csrftoken=");
        csrf2 = (csrf1[1].split(";"));
        csrftoken = csrf2[0];
        
    }
    
    public void sendPost(){
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(domain);
        HttpResponse response = null;
        
        request.addHeader("Cookie", "csrftoken="+csrftoken);
        request.addHeader("User-Agent", "Mozilla/5.0");
        
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("username", "user"));
        urlParameters.add(new BasicNameValuePair("password", "boku"));
        urlParameters.add(new BasicNameValuePair("csrfmiddlewaretoken", csrftoken));
        
        
      
     
        try {
            request.setEntity(new UrlEncodedFormEntity(urlParameters));
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        try {
            response = client.execute(request);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        Header[] h = null;
        h = response.getAllHeaders();
        //System.out.println(h[4].toString());
        for(int i=0; i < h.length; i++)
        {
        	Log.i(TAG, h[i].toString());
        }
        
        
    }

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		sendGet();
		sendPost();
		Log.i(TAG, csrftoken);
		return null;
	}

}
