package de.ak.labapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import de.ak.labapp.view.viewgroup.FlyOutContainer;

public class beActivity extends menuClick {

	private final String TAG = "BE_Activity";		// Tag zur genauern Indentifikation der Activity f�rs mitloggen
	FlyOutContainer root;
	private int sample;
	public String probenID;
	final Context context = this;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.be, null);
	    this.setContentView(root);
        Log.i(TAG, "on Create");
        
        globalFunktion probenID_g = ((globalFunktion)getApplicationContext());
        probenID = probenID_g.getprobenID();
        Log.i(TAG, "global:" + probenID);
        
        sample = 1;
        write_all(sample);
    }
   
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
		///////////////Initialisierung des Men�s////////////
		switch (item.getItemId()) {
	        
	    	case R.id.logout_menu :
	    		
	    		Log.i(TAG, "Logout is clicked");
	    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	    		// set title
	    		alertDialogBuilder.setTitle("Logout?");
	    		// set dialog message
	    		alertDialogBuilder
	    		.setMessage("Click yes to Logout!")
	    		.setCancelable(false)
	    		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    			// if this button is clicked, close
	    			// current activity
	    			finish();
	    			}
	    		})
	    		.setNegativeButton("No",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    				// if this button is clicked, just close
	    				// the dialog box and do nothing
	    						dialog.cancel();
	    				}
	    		});
	    		// create alert dialog
	    		AlertDialog alertDialog = alertDialogBuilder.create();
	    		// show it
	    		alertDialog.show();
	    		return true;
	    		
	        case R.id.info_menu :
	        	
	        	Log.i(TAG, "Info is clicked");
	            return true;
	            
	        case R.id.search:
	        	
	        	Log.i(TAG, "Search_Icon is clicked");
	        	
	        	Intent start_willkommen = new Intent(this, wellcome.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
	        	finish();
	        	startActivity(start_willkommen);											// neue Activity starten
	    	    Log.i(TAG,"neue Activity starten(wellcome)");
	        	return true;
	        	
	        case R.id.sidemenu:
	        	
	        	
			    this.root.toggleMenu();
	        	Log.i(TAG, "Sidebar is clicked");
	        	return true;
	            
	        default:
	        	return super.onOptionsItemSelected(item);
	    }
	}
    
    public void write_all(int i){
    	

    	String sample_name = " \n Sample: " + i  + " ProbenID = " + probenID + "\n";
    	TextView sample_number = (TextView) findViewById(R.id.sample_number_be);
    	sample_number.setText(sample_name);
    	getIntdata();
 		bioHttp biohttp = new bioHttp(csrf, session, sample_d);
    	
    	if(i==1){
    		String[] name = biohttp.getPlaceholder(i, "BE");
	    	EditText ho_bw = (EditText) findViewById(R.id.ho_bw_e_be);
	    	ho_bw.setHint(name[0]);
	    	ho_bw.setText("");
	    	EditText einw = (EditText) findViewById(R.id.einwaage_e_be);
	    	einw.setHint(name[1]);
	    	einw.setText("");
	    	EditText dratrest = (EditText) findViewById(R.id.dratrest_e_be);
	    	dratrest.setHint(name[2]);
	    	dratrest.setText("");
	    	EditText tara = (EditText) findViewById(R.id.tara_e_be);
	    	tara.setHint(name[3]);
	    	tara.setText("");
	    	EditText ausw = (EditText) findViewById(R.id.auswaage_e_be);
	    	ausw.setHint(name[4]);
	    	ausw.setText("");
	    	
	    	TextView result = (TextView) findViewById(R.id.result_be);
	    	result.setText("\n Result =" + name[5]);
	    	
	    	
	    	
    	}
    	
    	if(i==2){	
    		
    		String[] name = biohttp.getPlaceholder(i, "BE");
	    	EditText ho_bw = (EditText) findViewById(R.id.ho_bw_e_be);
	    	ho_bw.setHint(name[0]);
	    	ho_bw.setText("");
	    	EditText einw = (EditText) findViewById(R.id.einwaage_e_be);
	    	einw.setHint(name[1]);
	    	einw.setText("");
	    	EditText dratrest = (EditText) findViewById(R.id.dratrest_e_be);
	    	dratrest.setHint(name[2]);
	    	dratrest.setText("");
	    	EditText tara = (EditText) findViewById(R.id.tara_e_be);
	    	tara.setHint(name[3]);
	    	tara.setText("");
	    	EditText ausw = (EditText) findViewById(R.id.auswaage_e_be);
	    	ausw.setHint(name[4]);
	    	ausw.setText("");
	    	
	    	TextView result = (TextView) findViewById(R.id.result_be);
	    	result.setText("\n Result =" + name[5]);
	    	
	    	
    	}
    	
    	if(i==3){	
    		
    		String[] name = biohttp.getPlaceholder(i, "BE");
	    	EditText ho_bw = (EditText) findViewById(R.id.ho_bw_e_be);
	    	ho_bw.setHint(name[0]);
	    	ho_bw.setText("");
	    	EditText einw = (EditText) findViewById(R.id.einwaage_e_be);
	    	einw.setHint(name[1]);
	    	einw.setText("");
	    	EditText dratrest = (EditText) findViewById(R.id.dratrest_e_be);
	    	dratrest.setHint(name[2]);
	    	dratrest.setText("");
	    	EditText tara = (EditText) findViewById(R.id.tara_e_be);
	    	tara.setHint(name[3]);
	    	tara.setText("");
	    	EditText ausw = (EditText) findViewById(R.id.auswaage_e_be);
	    	ausw.setHint(name[4]);
	    	ausw.setText("");
	    	
	    	TextView result = (TextView) findViewById(R.id.result_be);
	    	result.setText("\n Result =" + name[5]);
	    	
    	}
    	
    }
    
    public void onClick_s1(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 1 (Nummer 1)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 1");
		sample = 1;
		write_all(sample);
    	 
    }
    
    public void onClick_s2(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 2 (Nummer 2)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 2");
		sample = 2;
		write_all(sample);
    	 
    }
 
    public void onClick_s3(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 3 (Nummer 3)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 3");
		sample = 3;
		write_all(sample);				// Placeholder der EditText-Felder setzen(neu: Werte die in der DatenBank stehen als Placeholder Setzen
 	 
    }
    
    public void onClick_submit(View view){
    //////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Handling zur �bertragung der Daten in die Datenbank			//
	//																						//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
    	
    	/////Daten Aus den EditText-Felder auslesen//////
    	
    	EditText ho_bw = (EditText) findViewById(R.id.ho_bw_e_be);
    	EditText einw = (EditText) findViewById(R.id.einwaage_e_be);
    	EditText dratrest = (EditText) findViewById(R.id.dratrest_e_be);
    	EditText tara = (EditText) findViewById(R.id.tara_e_be);
    	EditText ausw = (EditText) findViewById(R.id.auswaage_e_be);
    	
    	String probenID_send = probenID;
    	int Samplenumber_send = sample; 
    	String ho_bw_send = ho_bw.getText().toString();
    	String einw_send = einw.getText().toString();
    	String dratrest_send = dratrest.getText().toString();
    	String tara_send = tara.getText().toString();
    	String ausw_send = ausw.getText().toString();
    	/////////////////////////////////////////////////
    	getIntdata();
 		bioHttp biohttp = new bioHttp(csrf, session, sample_d);
 		biohttp.setBe(sample, ho_bw_send, einw_send, dratrest_send, tara_send, ausw_send);	
    	
    }
    
    public String csrf;
	public String sample_d;
	public String session;
	
    public void getIntdata(){
		
		globalFunktion setdata = ((globalFunktion)getApplicationContext());
        csrf = setdata.getcsrftoken();
        Log.i(TAG, "Csrf:" + csrf);
        session = setdata.getsession();
        Log.i(TAG, "Session:" + session);
        sample_d = setdata.getprobenID();
        Log.i(TAG, "Sample:" + sample_d);
		
	}

}
