package de.ak.labapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import de.ak.labapp.view.viewgroup.FlyOutContainer;

public class karlActivity extends menuClick {
	
	private final String TAG = "BE_Activity";		// Tag zur genauern Indentifikation der Activity f�rs mitloggen
	FlyOutContainer root;
	private int sample;
	public String probenID;
	final Context context = this;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.karl, null);
	    this.setContentView(root);
        Log.i(TAG, "on Create");
        
        globalFunktion probenID_g = ((globalFunktion)getApplicationContext());
        probenID = probenID_g.getprobenID();
        Log.i(TAG, "global:" + probenID);
        
        sample = 1;
        write_all(sample);
    }
   
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
		///////////////Initialisierung des Men�s////////////
		switch (item.getItemId()) {
	        
	    	case R.id.logout_menu :
	    		
	    		Log.i(TAG, "Logout is clicked");
	    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	    		// set title
	    		alertDialogBuilder.setTitle("Logout?");
	    		// set dialog message
	    		alertDialogBuilder
	    		.setMessage("Click yes to Logout!")
	    		.setCancelable(false)
	    		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    			// if this button is clicked, close
	    			// current activity
	    			finish();
	    			}
	    		})
	    		.setNegativeButton("No",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    				// if this button is clicked, just close
	    				// the dialog box and do nothing
	    						dialog.cancel();
	    				}
	    		});
	    		// create alert dialog
	    		AlertDialog alertDialog = alertDialogBuilder.create();
	    		// show it
	    		alertDialog.show();
	    		return true;
	    		
	        case R.id.info_menu :
	        	
	        	Log.i(TAG, "Info is clicked");
	            return true;
	            
	        case R.id.search:
	        	
	        	Log.i(TAG, "Search_Icon is clicked");
	        	
	        	Intent start_willkommen = new Intent(this, wellcome.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
	        	finish();
	        	startActivity(start_willkommen);											// neue Activity starten
	    	    Log.i(TAG,"neue Activity starten(wellcome)");
	        	return true;
	        	
	        case R.id.sidemenu:
	        	
	        	
			    this.root.toggleMenu();
	        	Log.i(TAG, "Sidebar is clicked");
	        	return true;
	            
	        default:
	        	return super.onOptionsItemSelected(item);
	    }
	}
    
    public void write_all(int i){
    	

    	String sample_name = " \n Sample: " + i  + " ProbenID = " + probenID + "\n";
    	TextView sample_number = (TextView) findViewById(R.id.sample_number_karl);
    	sample_number.setText(sample_name);
    	getIntdata();
 		bioHttp biohttp = new bioHttp(csrf, session, sample_d);
 		
    	if(i==1){
    		
    		String[] name = biohttp.getPlaceholder(i, "KarlFischer");
    		EditText einw = (EditText) findViewById(R.id.einwaage_e_karl);
	    	einw.setHint(name[0]);
	    	einw.setText("");
	    	EditText wkf = (EditText) findViewById(R.id.wkf_e_karl);
	    	wkf.setHint(name[1]);
	    	wkf.setText("");
	    	EditText wg = (EditText) findViewById(R.id.wassergehalt_e_karl);
	    	wg.setHint(name[2]);
	    	wg.setText("");
	    	EditText vm = (EditText) findViewById(R.id.v_methanol_e_karl);
	    	vm.setHint(name[3]);
	    	vm.setText("");	
	    	
    	}
    	
    	if(i==2){
    		
    		String[] name = biohttp.getPlaceholder(i, "KarlFischer");
    		EditText einw = (EditText) findViewById(R.id.einwaage_e_karl);
	    	einw.setHint(name[0]);
	    	einw.setText("");
	    	EditText wkf = (EditText) findViewById(R.id.wkf_e_karl);
	    	wkf.setHint(name[1]);
	    	wkf.setText("");
	    	EditText wg = (EditText) findViewById(R.id.wassergehalt_e_karl);
	    	wg.setHint(name[2]);
	    	wg.setText("");
	    	EditText vm = (EditText) findViewById(R.id.v_methanol_e_karl);
	    	vm.setHint(name[3]);
	    	vm.setText("");
	    		    	
    	}
    	
    	if(i==3){	
    		
    		String[] name = biohttp.getPlaceholder(i, "KarlFischer");
    		EditText einw = (EditText) findViewById(R.id.einwaage_e_karl);
	    	einw.setHint(name[0]);
	    	einw.setText("");
	    	EditText wkf = (EditText) findViewById(R.id.wkf_e_karl);
	    	wkf.setHint(name[1]);
	    	wkf.setText("");
	    	EditText wg = (EditText) findViewById(R.id.wassergehalt_e_karl);
	    	wg.setHint(name[2]);
	    	wg.setText("");
	    	EditText vm = (EditText) findViewById(R.id.v_methanol_e_karl);
	    	vm.setHint(name[3]);
	    	vm.setText("");
	    	
    	}
    	
    }
    
    public void onClick_s1(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 1 (Nummer 1)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 1");
		sample = 1;
		write_all(sample);
    	 
    }
    
    public void onClick_s2(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 2 (Nummer 2)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 2");
		sample = 2;
		write_all(sample);
    	 
    }
 
    public void onClick_s3(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 3 (Nummer 3)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 3");
		sample = 3;
		write_all(sample);				// Placeholder der EditText-Felder setzen(neu: Werte die in der DatenBank stehen als Placeholder Setzen
 	 
    }
    
    public void onClick_submit(View view){
    //////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Handling zur �bertragung der Daten in die Datenbank			//
	//																						//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
    	
    	/////Daten Aus den EditText-Felder auslesen//////
    	
    	EditText einw = (EditText) findViewById(R.id.einwaage_e_karl);
    	EditText wkf = (EditText) findViewById(R.id.wkf_e_karl);
    	EditText wassergehalt = (EditText) findViewById(R.id.wassergehalt_e_karl);
    	EditText methanol = (EditText) findViewById(R.id.v_methanol_e_karl);
    	
    	String probenID_send = probenID;
    	int Samplenumber_send = sample; 
    	String einw_send = einw.getText().toString();
    	String wkf_send = wkf.getText().toString();
    	String wassergehalt_send = wassergehalt.getText().toString();
    	String methanol_send = methanol.getText().toString();
    	/////////////////////////////////////////////////
    	getIntdata();
 		bioHttp biohttp = new bioHttp(csrf, session, sample_d);
 		biohttp.setKarli(sample, einw_send, wkf_send, wassergehalt_send, methanol_send);	
    	
    }
    
    public String csrf;
	public String sample_d;
	public String session;
	
    public void getIntdata(){
		
		globalFunktion setdata = ((globalFunktion)getApplicationContext());
        csrf = setdata.getcsrftoken();
        Log.i(TAG, "Csrf:" + csrf);
        session = setdata.getsession();
        Log.i(TAG, "Session:" + session);
        sample_d = setdata.getprobenID();
        Log.i(TAG, "Sample:" + sample_d);
		
	}


}
