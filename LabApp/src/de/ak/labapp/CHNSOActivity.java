package de.ak.labapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import de.ak.labapp.view.viewgroup.FlyOutContainer;

public class CHNSOActivity extends menuClick {
	
	private final String TAG = "CHNSO_Activity";		// Tag zur genauern Indentifikation der Activity f�rs mitloggen
	FlyOutContainer root;
	private int sample;
	public String probenID;
	final Context context = this;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.chnso, null);
	    this.setContentView(root);
        Log.i(TAG, "on Create");
        
        globalFunktion probenID_g = ((globalFunktion)getApplicationContext());
        probenID = probenID_g.getprobenID();
        Log.i(TAG, "global:" + probenID);
        
        sample = 1;
        write_all(sample);
    }
   
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
		///////////////Initialisierung des Men�s////////////
		switch (item.getItemId()) {
	        
	    	case R.id.logout_menu :
	    		
	    		Log.i(TAG, "Logout is clicked");
	    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	    		// set title
	    		alertDialogBuilder.setTitle("Logout?");
	    		// set dialog message
	    		alertDialogBuilder
	    		.setMessage("Click yes to Logout!")
	    		.setCancelable(false)
	    		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    			// if this button is clicked, close
	    			// current activity
	    			finish();
	    			}
	    		})
	    		.setNegativeButton("No",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    				// if this button is clicked, just close
	    				// the dialog box and do nothing
	    						dialog.cancel();
	    				}
	    		});
	    		// create alert dialog
	    		AlertDialog alertDialog = alertDialogBuilder.create();
	    		// show it
	    		alertDialog.show();
	    		return true;
	    		
	        case R.id.info_menu :
	        	
	        	Log.i(TAG, "Info is clicked");
	            return true;
	            
	        case R.id.search:
	        	
	        	Log.i(TAG, "Search_Icon is clicked");
	        	
	        	Intent start_willkommen = new Intent(this, wellcome.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
	        	finish();
	        	startActivity(start_willkommen);											// neue Activity starten
	    	    Log.i(TAG,"neue Activity starten(wellcome)");
	        	return true;
	        	
	        case R.id.sidemenu:
	        	
	        	
			    this.root.toggleMenu();
	        	Log.i(TAG, "Sidebar is clicked");
	        	return true;
	            
	        default:
	        	return super.onOptionsItemSelected(item);
	    }
	}
    
    public void write_all(int i){
    	

    	String sample_name = " \n Sample: " + i  + " ProbenID = " + probenID + "\n";
    	TextView sample_number = (TextView) findViewById(R.id.sample_number_chnso);
    	sample_number.setText(sample_name);
    	getIntdata();
 		bioHttp biohttp = new bioHttp(csrf, session, sample_d);
    	
    	if(i==1){
    		String[] name = biohttp.getPlaceholder(i, "CHNSO");
	    	EditText c_ts = (EditText) findViewById(R.id.c_ts_e_chnso);
	    	c_ts.setHint(name[0]);
	    	c_ts.setText("");
	    	EditText h_ts = (EditText) findViewById(R.id.h_ts_e_chnso);
	    	h_ts.setHint(name[1]);
	    	h_ts.setText("");
	    	EditText n_ts = (EditText) findViewById(R.id.n_ts_e_chnso);
	    	n_ts.setHint(name[2]);
	    	n_ts.setText("");
	    	EditText s_ts = (EditText) findViewById(R.id.s_ts_e_chnso);
	    	s_ts.setHint(name[3]);
	    	s_ts.setText("");
	    	EditText o_ts = (EditText) findViewById(R.id.o_ts_e_chnso);
	    	o_ts.setHint(name[4]);
	    	o_ts.setText("");
	    	EditText c_ots = (EditText) findViewById(R.id.c_ots_e_chnso);
	    	c_ots.setHint(name[5]);
	    	c_ots.setText("");
	    	EditText h_ots = (EditText) findViewById(R.id.h_ots_e_chnso);
	    	h_ots.setHint(name[6]);
	    	h_ots.setText("");
	    	EditText n_ots = (EditText) findViewById(R.id.n_ots_e_chnso);
	    	n_ots.setHint(name[7]);
	    	n_ots.setText("");
	    	EditText s_ots = (EditText) findViewById(R.id.s_ots_e_chnso);
	    	s_ots.setHint(name[8]);
	    	s_ots.setText("");
	    	EditText o_ots = (EditText) findViewById(R.id.o_ots_e_chnso);
	    	o_ots.setHint(name[9]);
	    	o_ots.setText("");
	    	    	
    	}
    	
    	if(i==2){
    		
    		String[] name = biohttp.getPlaceholder(i, "CHNSO");
	    	EditText c_ts = (EditText) findViewById(R.id.c_ts_e_chnso);
	    	c_ts.setHint(name[0]);
	    	c_ts.setText("");
	    	EditText h_ts = (EditText) findViewById(R.id.h_ts_e_chnso);
	    	h_ts.setHint(name[1]);
	    	h_ts.setText("");
	    	EditText n_ts = (EditText) findViewById(R.id.n_ts_e_chnso);
	    	n_ts.setHint(name[2]);
	    	n_ts.setText("");
	    	EditText s_ts = (EditText) findViewById(R.id.s_ts_e_chnso);
	    	s_ts.setHint(name[3]);
	    	s_ts.setText("");
	    	EditText o_ts = (EditText) findViewById(R.id.o_ts_e_chnso);
	    	o_ts.setHint(name[4]);
	    	o_ts.setText("");
	    	EditText c_ots = (EditText) findViewById(R.id.c_ots_e_chnso);
	    	c_ots.setHint(name[5]);
	    	c_ots.setText("");
	    	EditText h_ots = (EditText) findViewById(R.id.h_ots_e_chnso);
	    	h_ots.setHint(name[6]);
	    	h_ots.setText("");
	    	EditText n_ots = (EditText) findViewById(R.id.n_ots_e_chnso);
	    	n_ots.setHint(name[7]);
	    	n_ots.setText("");
	    	EditText s_ots = (EditText) findViewById(R.id.s_ots_e_chnso);
	    	s_ots.setHint(name[8]);
	    	s_ots.setText("");
	    	EditText o_ots = (EditText) findViewById(R.id.o_ots_e_chnso);
	    	o_ots.setHint(name[9]);
	    	o_ots.setText("");  	
	    	
    	}
    	
    	if(i==3){
    		
    		String[] name = biohttp.getPlaceholder(i, "CHNSO");
	    	EditText c_ts = (EditText) findViewById(R.id.c_ts_e_chnso);
	    	c_ts.setHint(name[0]);
	    	c_ts.setText("");
	    	EditText h_ts = (EditText) findViewById(R.id.h_ts_e_chnso);
	    	h_ts.setHint(name[1]);
	    	h_ts.setText("");
	    	EditText n_ts = (EditText) findViewById(R.id.n_ts_e_chnso);
	    	n_ts.setHint(name[2]);
	    	n_ts.setText("");
	    	EditText s_ts = (EditText) findViewById(R.id.s_ts_e_chnso);
	    	s_ts.setHint(name[3]);
	    	s_ts.setText("");
	    	EditText o_ts = (EditText) findViewById(R.id.o_ts_e_chnso);
	    	o_ts.setHint(name[4]);
	    	o_ts.setText("");
	    	EditText c_ots = (EditText) findViewById(R.id.c_ots_e_chnso);
	    	c_ots.setHint(name[5]);
	    	c_ots.setText("");
	    	EditText h_ots = (EditText) findViewById(R.id.h_ots_e_chnso);
	    	h_ots.setHint(name[6]);
	    	h_ots.setText("");
	    	EditText n_ots = (EditText) findViewById(R.id.n_ots_e_chnso);
	    	n_ots.setHint(name[7]);
	    	n_ots.setText("");
	    	EditText s_ots = (EditText) findViewById(R.id.s_ots_e_chnso);
	    	s_ots.setHint(name[8]);
	    	s_ots.setText("");
	    	EditText o_ots = (EditText) findViewById(R.id.o_ots_e_chnso);
	    	o_ots.setHint(name[9]);
	    	o_ots.setText("");  	
    	}
    	
    }
    
    public void onClick_s1(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 1 (Nummer 1)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 1");
		sample = 1;
		write_all(sample);
    	 
    }
    
    public void onClick_s2(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 2 (Nummer 2)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 2");
		sample = 2;
		write_all(sample);
    	 
    }
 
    public void onClick_s3(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 3 (Nummer 3)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 3");
		sample = 3;
		write_all(sample);				// Placeholder der EditText-Felder setzen(neu: Werte die in der DatenBank stehen als Placeholder Setzen
 	 
    }
    
    public void onClick_submit(View view){
    //////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Handling zur �bertragung der Daten in die Datenbank			//
	//																						//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
    	
    	/////Daten Aus den EditText-Felder auslesen//////
    	
    	EditText c_ts = (EditText) findViewById(R.id.c_ts_e_chnso);
    	EditText h_ts = (EditText) findViewById(R.id.h_ts_e_chnso);
    	EditText n_ts = (EditText) findViewById(R.id.n_ts_e_chnso);
    	EditText s_ts = (EditText) findViewById(R.id.s_ts_e_chnso);
    	EditText o_ts = (EditText) findViewById(R.id.o_ts_e_chnso);
    	
    	EditText c_ots = (EditText) findViewById(R.id.c_ots_e_chnso);
    	EditText h_ots = (EditText) findViewById(R.id.h_ots_e_chnso);
    	EditText n_ots = (EditText) findViewById(R.id.n_ots_e_chnso);
    	EditText s_ots = (EditText) findViewById(R.id.s_ots_e_chnso);
    	EditText o_ots = (EditText) findViewById(R.id.o_ots_e_chnso);
    	
    	
    	String probenID_send = probenID;
    	int Samplenumber_send = sample; 
    	
    	String c_ts_send = c_ts.getText().toString();
    	String h_ts_send = h_ts.getText().toString();
    	String n_ts_send = n_ts.getText().toString();
    	String s_ts_send = s_ts.getText().toString();
    	String o_ts_send = o_ts.getText().toString();
    	
    	String c_ots_send = c_ots.getText().toString();
    	String h_ots_send = h_ots.getText().toString();
    	String n_ots_send = n_ots.getText().toString();
    	String s_ots_send = s_ots.getText().toString();
    	String o_ots_send = o_ots.getText().toString();
    	
    	/////////////////////////////////////////////////
    	getIntdata();
 		bioHttp biohttp = new bioHttp(csrf, session, sample_d);
 		biohttp.setchnso(sample, c_ts_send, h_ts_send, n_ts_send, s_ts_send, o_ts_send, c_ots_send, h_ots_send, n_ots_send, s_ots_send, o_ots_send);	 	
    	
    }
    public String csrf;
	public String sample_d;
	public String session;
	
    public void getIntdata(){
		
		globalFunktion setdata = ((globalFunktion)getApplicationContext());
        csrf = setdata.getcsrftoken();
        Log.i(TAG, "Csrf:" + csrf);
        session = setdata.getsession();
        Log.i(TAG, "Session:" + session);
        sample_d = setdata.getprobenID();
        Log.i(TAG, "Sample:" + sample_d);
		
	}

}
