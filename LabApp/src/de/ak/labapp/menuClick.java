package de.ak.labapp;

import de.ak.labapp.view.viewgroup.FlyOutContainer;
import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

public class menuClick extends Activity implements OnClickListener {
	public String PROBENID;
	private final String TAG = "menuclick";		// Tag zur genauern Indentifikation der Activity f�rs mitloggen
	//FlyOutContainer root;
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
    	MenuInflater inflate = getMenuInflater();
        getMenuInflater().inflate(R.menu.analyse_menu, menu);
        return true;
    }
	
	

///////////////////////////////////Start/Stopp/Resume/restart/pause f�r die Activitys der Klasse//////////////////
///////////////////////////////////Sind hier angef�hr um mit zu Loggen!///////////////////////////////////////////
    @Override
    public void onStart(){
    	
    	super.onStart();
    	Log.i(TAG, "onStart");			//Log Befehl zum mitloggen
    }
    @Override
    public void onStop(){
    	
    	super.onStop();
    	Log.i(TAG, "onStopped");		//Log Befehl zum mitloggen
    } 
    @Override
    public void onResume(){
    	
    	super.onResume();
    	Log.i(TAG, "onResume");				//Log Befehl zum mitloggen
    }
    @Override
    public void onRestart(){
    	
    	super.onRestart();
    	Log.i(TAG, "onRestart");			//Log Befehl zum mitloggen
    }
    @Override
    public void onPause(){
    	
    	super.onPause();
    	Log.i(TAG, "onPause");			//Log Befehl zum mitloggen
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
	public void onClick_a1(View view){
    	
    	Log.i(TAG, "Analyse1 is clicked");
    	finish();
    	Log.i(TAG, "wird vernichtet");
    	Intent start_analyse1 = new Intent(this, analyse1Activity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse1Activity)
    	start_analyse1.putExtra("new",2);
    	startActivity(start_analyse1);											// neue Activity starten
	    Log.i(TAG,"neue Activity starten(analyse1)");							// Informationslog 
    }
    
	 public void onClick_a2(View view){
	    	
	    	Log.i(TAG, "Analyse2 is clicked");
	    	finish();
	    	Log.i(TAG, "wird vernichtet");
	    	Intent start_analyse2 = new Intent(this, analyse2Activity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
	    	startActivity(start_analyse2);											// neue Activity starten
		    Log.i(TAG,"neue Activity starten(Analyse2)");							// Informationslog 
	 }
	 
	 public void onClick_a3(View view){
		 	
		 	Log.i(TAG, "Analyse3 is clicked");
		 	finish();
	    	Log.i(TAG, "wird vernichtet");
	    	Intent start_analyse3 = new Intent(this, analyse3Activity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
	    	startActivity(start_analyse3);											// neue Activity starten
		    Log.i(TAG,"neue Activity starten(Analyse3)");							// Informationslog 
		 }
		 
		 public void onClick_a4(View view){
		    	
		    	Log.i(TAG, "Analyse4 is clicked");
		    	finish();
		    	Log.i(TAG, "wird vernichtet");
		    	Intent start_analyse4 = new Intent(this, analyse4Activity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
		    	startActivity(start_analyse4);											// neue Activity starten
			    Log.i(TAG,"neue Activity starten(Analyse4)");							// Informationslog 
		    }
		 public void onClick_a5(View view){
		    	
		    	Log.i(TAG, "Analyse5 is clicked");
		    	finish();
		    	Log.i(TAG, "wird vernichtet");
		    	Intent start_analyse5 = new Intent(this, analyse5Activity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
		    	startActivity(start_analyse5);											// neue Activity starten
			    Log.i(TAG,"neue Activity starten(Analyse5)");							// Informationslog
		    }
		 
		 public void onClick_be(View view){
		    	
		    	Log.i(TAG, "BE is clicked");
		    	
		    	finish();
		    	Log.i(TAG, "wird vernichtet");
		    	Intent start_be = new Intent(this, beActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
		    	startActivity(start_be);											// neue Activity starten
			    Log.i(TAG,"neue Activity starten(BE)");							// Informationslog 
		    }
		 
		 public void onClick_chnso(View view){
		    	
		    	Log.i(TAG, "CHNSO is clicked");
		    	
		    	finish();
		    	Log.i(TAG, "wird vernichtet");
		    	Intent start_chnso = new Intent(this, CHNSOActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
		    	startActivity(start_chnso);											// neue Activity starten
			    Log.i(TAG,"neue Activity starten(CHNSO)");							// Informationslog 
		    }
		 
		 public void onClick_csb(View view){
		    	
		    	Log.i(TAG, "CSB is clicked");
		    	
		    	finish();
		    	Log.i(TAG, "wird vernichtet");
		    	Intent start_csb = new Intent(this, csbActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
		    	startActivity(start_csb);											// neue Activity starten
			    Log.i(TAG,"neue Activity starten(CSB)");							// Informationslog 
		    }
		 
		 public void onClick_fatty(View view){
		    	
		    	Log.i(TAG, "Fatty is clicked");
		    	finish();
		    	Log.i(TAG, "wird vernichtet");
		    	Intent start_fatty = new Intent(this, fattyActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
		    	startActivity(start_fatty);											// neue Activity starten
			    Log.i(TAG,"neue Activity starten(Fatty Acids)");							// Informationslog 
		    }

		 
		 public void onClick_silage(View view){
		    	
		    	Log.i(TAG, "Silage is clicked");
		    	finish();
		    	Log.i(TAG, "wird vernichtet");
		    	Intent start_silage = new Intent(this, silagequaliActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
		    	startActivity(start_silage);											// neue Activity starten
			    Log.i(TAG,"neue Activity starten(Silagequalit�t)");							// Informationslog 
		    }

		 
		 public void onClick_karl(View view){
		    	
		    	Log.i(TAG, "Karl is clicked");
		    	finish();
		    	Log.i(TAG, "wird vernichtet");
		    	Intent start_karl = new Intent(this, karlActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity analyse2Activity)
		    	startActivity(start_karl);											// neue Activity starten
			    Log.i(TAG,"neue Activity starten(Karl Fischer)");							// Informationslog 
		    }
		 
		 public void onClick_ts(View view){
		    	
		    	Log.i(TAG, "TS is clicked");
		    	
		    	Intent start_analyse2 = new Intent(this, MainActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
				startActivity(start_analyse2);									// neue Activity starten
			    Log.i(TAG,"neue Activity starten(Main)");			// Informationslog 
		    }
		 
		 public void onClick_nt(View view){
		    	
		    	Log.i(TAG, "NT is clicked");
		    	
		    	Intent start_analyse2 = new Intent(this, MainActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
				startActivity(start_analyse2);									// neue Activity starten
			    Log.i(TAG,"neue Activity starten(Main)");			// Informationslog 
		    }
		 public void onClick_nh4(View view){
		    	
		    	Log.i(TAG, "NH4 is clicked");
		    	
		    	Intent start_analyse2 = new Intent(this, MainActivity.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
				startActivity(start_analyse2);									// neue Activity starten
			    Log.i(TAG,"neue Activity starten(Main)");			// Informationslog 
		    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	

}
