package de.ak.labapp;

import android.app.Application;

public class globalFunktion extends Application {

	 private String PROBENID = "00-000";
	 private String Session = "";
	 private String csrftoken = "";
	 private String sample = "";
	 //public bioHttp http = new bioHttp();
	 

	 public String getprobenID(){
	    return PROBENID;
	 }
	 
	 public void setprobenID(String p){
	   
		 PROBENID = p;
		 sample = p;
	 }
	 
	 public String getsession(){
		 
		    return Session;
	 }
		 
	 public void setsession(String p){
		   
			 Session = p;
	 }
	 
	 public String getcsrftoken(){
		 
		    return csrftoken;
	 }
		 
	 public void setcsrftoken(String p){
		   
			 csrftoken = p;
	 }
	 public String getsample(){
		 
		    return sample;
	 }
		 
	 public void setsample(String p){
		   
			 sample = p;
	 }
}
