package de.ak.labapp;


import java.util.concurrent.ExecutionException;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity{ 

	private final String TAG = "MainActivity";				// Tag zur genauern Indentifikation der Activity f�rs mitloggen
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	////////////Startet die Activity///////////////////////
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Log.i(TAG, "on Create");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
    	MenuInflater inflate = getMenuInflater();
        getMenuInflater().inflate(R.menu.main, menu);			//	Intitialisiert das Men�
        return true;
    }

///////////////////////////////////Start/Stopp/Resume/restart/pause f�r die Activitys der Klasse//////////////////
///////////////////////////////////Sind hier angef�hr um mit zu Loggen!///////////////////////////////////////////
	@Override
	public void onStart(){
	
		super.onStart();
		Log.i(TAG, "onStart");			//Log Befehl zum mitloggen
	}
	@Override
	public void onStop(){
	
		super.onStop();
		Log.i(TAG, "onStopped");		//Log Befehl zum mitloggen
	} 
	@Override
	public void onResume(){
	
		super.onResume();
		Log.i(TAG, "onResume");				//Log Befehl zum mitloggen
	}
	@Override
	public void onRestart(){
	
		super.onRestart();
		Log.i(TAG, "onRestart");			//Log Befehl zum mitloggen
	}
	@Override
	public void onPause(){
	
		super.onPause();
		Log.i(TAG, "onPause");			//Log Befehl zum mitloggen
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    @Override
	public boolean onOptionsItemSelected(MenuItem item){
		///////////////Initialisierung des Men�s////////////
		switch (item.getItemId()) {
	    		
	        case R.id.info_menu :
	            return true;
	            
	        default:
	        	return super.onOptionsItemSelected(item);
	    }
	}
	

    public void onClick_login(View view) {
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r den Login Button													//
	//handelt den Login(Server-Conection) und stellt eine Verbindung her(setzt Cookie)		//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
    	Log.i(TAG,"Login Button clicked");
    	
    	//http2 http = new http2();
        EditText benutzer;
        EditText password;
        
        benutzer = (EditText) findViewById(R.id.login_username);
        password = (EditText) findViewById(R.id.login_pw);
        String username = benutzer.getText().toString();
        String pw = password.getText().toString();
        Log.i(TAG, username);
        Log.i(TAG, pw);
        	
        bioHttp biohttp = new bioHttp();
        int login_s;
      
        login_s = biohttp.login(username, pw);
        Log.i(TAG, "login:" + login_s);
        Log.i(TAG,"CSRF = " +biohttp.csrftoken);
        Log.i(TAG,"Session = " +biohttp.session);
        setIntdata(biohttp.csrftoken, biohttp.session, biohttp.sample);
             

             
             
        if(login_s == 0){
             	
          Intent intent = new Intent(this, wellcome.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
          startActivity(intent);									// neue Activity starten
          Log.i(TAG,"neue Activity starten(wellcome)");			// Informationslog  	
        }else{
             	
        	Log.i(TAG, "Login Fail");
            EditText passw = (EditText) findViewById(R.id.login_pw);
            passw.setHint("Password");
            passw.setText("");
     	   	TextView fail = (TextView) findViewById(R.id.note_login);
     	    fail.setText("Fail during the connection");
             }
        }   
    
    public void setIntdata(String csrf, String session, String sample){
		
		globalFunktion setdata = ((globalFunktion)getApplicationContext());
        setdata.setcsrftoken(csrf);
        setdata.setsession(session);
        setdata.setsample(sample);		
	}
}
