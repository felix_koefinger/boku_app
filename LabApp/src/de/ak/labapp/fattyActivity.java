package de.ak.labapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import de.ak.labapp.view.viewgroup.FlyOutContainer;

public class fattyActivity extends menuClick {
	

	private final String TAG = "Fatty_Activity";		// Tag zur genauern Indentifikation der Activity f�rs mitloggen
	FlyOutContainer root;
	private int sample;
	public String probenID;
	final Context context = this;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.fatty, null);
	    this.setContentView(root);
        Log.i(TAG, "on Create");
        
        globalFunktion probenID_g = ((globalFunktion)getApplicationContext());
        probenID = probenID_g.getprobenID();
        Log.i(TAG, "global:" + probenID);
        
        sample = 1;
        write_all(sample);
    }
   
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
		///////////////Initialisierung des Men�s////////////
		switch (item.getItemId()) {
	        
	    	case R.id.logout_menu :
	    		
	    		Log.i(TAG, "Logout is clicked");
	    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	    		// set title
	    		alertDialogBuilder.setTitle("Logout?");
	    		// set dialog message
	    		alertDialogBuilder
	    		.setMessage("Click yes to Logout!")
	    		.setCancelable(false)
	    		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    			// if this button is clicked, close
	    			// current activity
	    			finish();
	    			}
	    		})
	    		.setNegativeButton("No",new DialogInterface.OnClickListener() {
	    			public void onClick(DialogInterface dialog,int id) {
	    				// if this button is clicked, just close
	    				// the dialog box and do nothing
	    						dialog.cancel();
	    				}
	    		});
	    		// create alert dialog
	    		AlertDialog alertDialog = alertDialogBuilder.create();
	    		// show it
	    		alertDialog.show();
	    		return true;
	    		
	        case R.id.info_menu :
	        	
	        	Log.i(TAG, "Info is clicked");
	            return true;
	            
	        case R.id.search:
	        	
	        	Log.i(TAG, "Search_Icon is clicked");
	        	
	        	Intent start_willkommen = new Intent(this, wellcome.class);		// neue Klasse von Intent (parameter f�r die Neue Activity wellcome)
	        	finish();
	        	startActivity(start_willkommen);											// neue Activity starten
	    	    Log.i(TAG,"neue Activity starten(wellcome)");
	        	return true;
	        	
	        case R.id.sidemenu:
	        	
	        	
			    this.root.toggleMenu();
	        	Log.i(TAG, "Sidebar is clicked");
	        	return true;
	            
	        default:
	        	return super.onOptionsItemSelected(item);
	    }
	}
    
    public void write_all(int i){
    	

    	String sample_name = " \n Sample: " + i  + " ProbenID = " + probenID + "\n";
    	TextView sample_number = (TextView) findViewById(R.id.sample_number_fat);
    	sample_number.setText(sample_name);
    	getIntdata();
 		bioHttp biohttp = new bioHttp(csrf, session, sample_d);
    	
    	if(i==1){
    		
    		String[] name = biohttp.getPlaceholder(i, "Fettsaeuren");
	    	EditText hac = (EditText) findViewById(R.id.hac_e_fat);
	    	hac.setHint(name[0]);
	    	hac.setText("");
	    	EditText pro = (EditText) findViewById(R.id.pro_e_fat);
	    	pro.setHint(name[1]);
	    	pro.setText("");
	    	EditText ibut = (EditText) findViewById(R.id.i_but_e_fat);
	    	ibut.setHint(name[2]);
	    	ibut.setText("");
	    	EditText nbut = (EditText) findViewById(R.id.n_but_e_fat);
	    	nbut.setHint(name[3]);
	    	nbut.setText("");
	    	EditText ival = (EditText) findViewById(R.id.i_val_e_fat);
	    	ival.setHint(name[4]);
	    	ival.setText("");
	    	EditText nval = (EditText) findViewById(R.id.n_val_e_fat);
	    	nval.setHint(name[5]);
	    	nval.setText("");
	    	EditText cap = (EditText) findViewById(R.id.cap_e_fat);
	    	cap.setHint(name[6]);
	    	cap.setText("");
	    	EditText hacpro = (EditText) findViewById(R.id.hac_pro_e_fat);
	    	hacpro.setHint(name[7]);
	    	hacpro.setText("");
	    	EditText note = (EditText) findViewById(R.id.note_e_fat);
	    	note.setHint(name[8]);
	    	note.setText("");
	    	
	    	
	    	
    	}
    	
    	if(i==2){	

    		String[] name = biohttp.getPlaceholder(i, "Fettsaeuren");
	    	EditText hac = (EditText) findViewById(R.id.hac_e_fat);
	    	hac.setHint(name[0]);
	    	hac.setText("");
	    	EditText pro = (EditText) findViewById(R.id.pro_e_fat);
	    	pro.setHint(name[1]);
	    	pro.setText("");
	    	EditText ibut = (EditText) findViewById(R.id.i_but_e_fat);
	    	ibut.setHint(name[2]);
	    	ibut.setText("");
	    	EditText nbut = (EditText) findViewById(R.id.n_but_e_fat);
	    	nbut.setHint(name[3]);
	    	nbut.setText("");
	    	EditText ival = (EditText) findViewById(R.id.i_val_e_fat);
	    	ival.setHint(name[4]);
	    	ival.setText("");
	    	EditText nval = (EditText) findViewById(R.id.n_val_e_fat);
	    	nval.setHint(name[5]);
	    	nval.setText("");
	    	EditText cap = (EditText) findViewById(R.id.cap_e_fat);
	    	cap.setHint(name[6]);
	    	cap.setText("");
	    	EditText hacpro = (EditText) findViewById(R.id.hac_pro_e_fat);
	    	hacpro.setHint(name[7]);
	    	hacpro.setText("");
	    	EditText note = (EditText) findViewById(R.id.note_e_fat);
	    	note.setHint(name[8]);
	    	note.setText("");
	    	
	    	
    	}
    	
    	if(i==3){	

    		String[] name = biohttp.getPlaceholder(i, "Fettsaeuren");
	    	EditText hac = (EditText) findViewById(R.id.hac_e_fat);
	    	hac.setHint(name[0]);
	    	hac.setText("");
	    	EditText pro = (EditText) findViewById(R.id.pro_e_fat);
	    	pro.setHint(name[1]);
	    	pro.setText("");
	    	EditText ibut = (EditText) findViewById(R.id.i_but_e_fat);
	    	ibut.setHint(name[2]);
	    	ibut.setText("");
	    	EditText nbut = (EditText) findViewById(R.id.n_but_e_fat);
	    	nbut.setHint(name[3]);
	    	nbut.setText("");
	    	EditText ival = (EditText) findViewById(R.id.i_val_e_fat);
	    	ival.setHint(name[4]);
	    	ival.setText("");
	    	EditText nval = (EditText) findViewById(R.id.n_val_e_fat);
	    	nval.setHint(name[5]);
	    	nval.setText("");
	    	EditText cap = (EditText) findViewById(R.id.cap_e_fat);
	    	cap.setHint(name[6]);
	    	cap.setText("");
	    	EditText hacpro = (EditText) findViewById(R.id.hac_pro_e_fat);
	    	hacpro.setHint(name[7]);
	    	hacpro.setText("");
	    	EditText note = (EditText) findViewById(R.id.note_e_fat);
	    	note.setHint(name[8]);
	    	note.setText("");
	   
    	}
    	
    }
    
    public void onClick_s1(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 1 (Nummer 1)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 1");
		sample = 1;
		write_all(sample);
    	 
    }
    
    public void onClick_s2(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 2 (Nummer 2)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 2");
		sample = 2;
		write_all(sample);
    	 
    }
 
    public void onClick_s3(View view){
	//////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Switchen von den Sampels einer Probe							//
	//Sampel: 3 (Nummer 3)																	//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
		Log.i(TAG, "Sample Button 3");
		sample = 3;
		write_all(sample);				// Placeholder der EditText-Felder setzen(neu: Werte die in der DatenBank stehen als Placeholder Setzen
 	 
    }
    
    public void onClick_submit(View view){
    //////////////////////////////////////////////////////////////////////////////////////////
	//																						//
	//onClick Methode f�r das Handling zur �bertragung der Daten in die Datenbank			//
	//																						//
	//																						//
	//////////////////////////////////////////////////////////////////////////////////////////
    	
    	/////Daten Aus den EditText-Felder auslesen//////
    	
    	EditText hac = (EditText) findViewById(R.id.hac_e_fat);
    	EditText pro = (EditText) findViewById(R.id.pro_e_fat);
    	EditText ibut = (EditText) findViewById(R.id.i_but_e_fat);
    	EditText nbut = (EditText) findViewById(R.id.n_but_e_fat);
    	
    	EditText ival = (EditText) findViewById(R.id.i_val_e_fat);
    	EditText nval = (EditText) findViewById(R.id.n_val_e_fat);
    	EditText cap = (EditText) findViewById(R.id.cap_e_fat);
    	EditText hacpro = (EditText) findViewById(R.id.hac_pro_e_fat);
    	EditText note = (EditText) findViewById(R.id.note_e_fat);
    	
    	
    	String probenID_send = probenID;
    	int Samplenumber_send = sample; 
    	
    	String hac_send = hac.getText().toString();
    	String pro_send = pro.getText().toString();
    	String ibut_send = ibut.getText().toString();
    	String nbut_send = nbut.getText().toString();
    	String ival_send = ival.getText().toString();
    	String nval_send = nval.getText().toString();
    	String cap_send = cap.getText().toString();
    	String hacpro_send = hacpro.getText().toString();
    	String note_send = note.getText().toString();  	
    	/////////////////////////////////////////////////
    	getIntdata();
 		bioHttp biohttp = new bioHttp(csrf, session, sample_d);
 		biohttp.setfatty(sample, hac_send, pro_send, ibut_send, nbut_send, ival_send, nval_send, cap_send, hacpro_send, note_send); 	
    	
    }

    public String csrf;
	public String sample_d;
	public String session;
	
    public void getIntdata(){
		
		globalFunktion setdata = ((globalFunktion)getApplicationContext());
        csrf = setdata.getcsrftoken();
        Log.i(TAG, "Csrf:" + csrf);
        session = setdata.getsession();
        Log.i(TAG, "Session:" + session);
        sample_d = setdata.getprobenID();
        Log.i(TAG, "Sample:" + sample_d);
		
	}
}
